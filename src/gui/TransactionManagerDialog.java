/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.toedter.calendar.JDateChooser;
import controller.ControllerTransaction;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import model.entity.Category;
import model.entity.CategoryEnum;
import model.entity.Transaction;


/**
 *
 * @author
 */
public class TransactionManagerDialog extends javax.swing.JDialog implements TableModelListener {

    private ControllerTransaction contTrans;
    private String comboBoxText;
    private DefaultTableModel dtm;
    private Object newValue;
    private Optional<Category> selectedCateg;
    private Transaction trans;
    private Optional<LocalDate> startDate;
    private Optional<LocalDate> endDate;
    private EnumMap<CategoryEnum, Integer> enumMap;
    
    
    public TransactionManagerDialog(java.awt.Frame parent, ControllerTransaction contTrans) {
        super(parent, true);
        initComponents();
        setLocationRelativeTo(parent);
        
        this.contTrans = contTrans;

        dtm = (DefaultTableModel) transactionsTable.getModel();
        dtm.addTableModelListener(this);

        comboBoxText = "Válassz kategóriát!";
        allTransactionsButtonActionPerformed(null);

        categoriesComboBoxUpdate(contTrans.getAllCategoriesBySort());
        centerHeadersDateColumn();
        setPeriodTextFields();
        alignTextFieldsRight();
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        categoriesComboBox = new javax.swing.JComboBox();
        startDateChooser = new com.toedter.calendar.JDateChooser();
        endDateChooser = new com.toedter.calendar.JDateChooser();
        okButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        transactionsTable = new javax.swing.JTable();
        addIncomeTransButton = new javax.swing.JButton();
        addExpenseTransButton = new javax.swing.JButton();
        modifyTransButton = new javax.swing.JButton();
        deleteTransButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        totalAmountCategoryTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        periodExpenseTextField = new javax.swing.JTextField();
        periodBalanceTextField = new javax.swing.JTextField();
        allTransactionsButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        periodIncomeTextField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tranzakciók szerkesztése, lekérdezése");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("A szűréshez adj meg kategóriát és / vagy dátumot:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("(üresen mindent listáz)");

        jLabel3.setText("Kategória:");

        jLabel4.setText("Dátumtól:");

        jLabel5.setText("Dátumig:");

        categoriesComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoriesComboBoxActionPerformed(evt);
            }
        });

        startDateChooser.setDateFormatString("yyyy-MM-dd");

        endDateChooser.setDateFormatString("yyyy-MM-dd");

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        transactionsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", null, null}
            },
            new String [] {
                "Kategória", "Összeg", "Dátum"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        transactionsTable.setShowHorizontalLines(false);
        jScrollPane1.setViewportView(transactionsTable);

        addIncomeTransButton.setText("Bevétel hozzáadása");
        addIncomeTransButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIncomeTransButtonActionPerformed(evt);
            }
        });

        addExpenseTransButton.setText("Kiadás hozzáadása");
        addExpenseTransButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addExpenseTransButtonActionPerformed(evt);
            }
        });

        modifyTransButton.setText("Módosítás");
        modifyTransButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifyTransButtonActionPerformed(evt);
            }
        });

        deleteTransButton.setText("Törlés");
        deleteTransButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteTransButtonActionPerformed(evt);
            }
        });

        jLabel6.setText("A kiválasztott kategória összesen:");

        totalAmountCategoryTextField.setEditable(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("A kiválasztott időszak:");

        jLabel8.setText("Bevételei:");

        jLabel9.setText("Kiadásai:");

        jLabel10.setText("Egyenlege:");

        periodExpenseTextField.setEditable(false);

        periodBalanceTextField.setEditable(false);

        allTransactionsButton.setText("Összes");
        allTransactionsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allTransactionsButtonActionPerformed(evt);
            }
        });

        closeButton.setText("Bezárás");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        periodIncomeTextField.setEditable(false);
        periodIncomeTextField.setText("jTextField1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(categoriesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(allTransactionsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(endDateChooser, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(startDateChooser, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(okButton))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(addIncomeTransButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(addExpenseTransButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(modifyTransButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(deleteTransButton, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(42, 42, 42)
                                        .addComponent(closeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(22, 22, 22))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(totalAmountCategoryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10)
                                            .addComponent(jLabel8))
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(periodBalanceTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                            .addComponent(periodExpenseTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                            .addComponent(periodIncomeTextField))))))
                        .addContainerGap(272, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(categoriesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(allTransactionsButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(startDateChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(okButton)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(endDateChooser, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(addIncomeTransButton)
                        .addGap(18, 18, 18)
                        .addComponent(addExpenseTransButton)
                        .addGap(18, 18, 18)
                        .addComponent(modifyTransButton)
                        .addGap(18, 18, 18)
                        .addComponent(deleteTransButton)
                        .addGap(18, 18, 18)
                        .addComponent(closeButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(totalAmountCategoryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(periodIncomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(periodExpenseTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(periodBalanceTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(26, 26, 26))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void allTransactionsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allTransactionsButtonActionPerformed

        selectedCateg = Optional.empty();
        categoriesComboBox.getModel().setSelectedItem(comboBoxText);
        tableUpdate(contTrans.getAllTransactionsDescByDate());
        totalAmountCategoryTextField.setText("");
        
        startDateChooser.setDate(null);
        startDate = Optional.empty();
        
        endDateChooser.setDate(null);
        endDate = Optional.empty();
        
        setPeriodTextFields();

    }//GEN-LAST:event_allTransactionsButtonActionPerformed

    private void categoriesComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoriesComboBoxActionPerformed

        if (categoriesComboBox.getModel().getSelectedItem() != null
                && !categoriesComboBox.getModel().getSelectedItem().equals(comboBoxText)) {
            Category selectedCtg = (Category) categoriesComboBox.getSelectedItem();
            selectedCateg = Optional.ofNullable(selectedCtg);
            tableUpdate(contTrans.getAllTransByCategoryAndDate(selectedCateg, startDate, endDate));
            setTotalAmountCategoryTextField();
            setPeriodTextFields();
        }
        
    }//GEN-LAST:event_categoriesComboBoxActionPerformed

    private void addIncomeTransButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIncomeTransButtonActionPerformed
        
        contTrans.openTransMngrAddNewIncDialog();
        
    }//GEN-LAST:event_addIncomeTransButtonActionPerformed

    private void addExpenseTransButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addExpenseTransButtonActionPerformed
        
        contTrans.openTransMngrAddNewExpDialog();
        
    }//GEN-LAST:event_addExpenseTransButtonActionPerformed

    private void modifyTransButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifyTransButtonActionPerformed
        
        contTrans.openTransMngrModifyDialog();
        
    }//GEN-LAST:event_modifyTransButtonActionPerformed

    private void deleteTransButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteTransButtonActionPerformed
        
        contTrans.deleteButtonTransMngrDialog();
        setTotalAmountCategoryTextField();
        setPeriodTextFields();
        tableUpdate(contTrans.getTransList());
        
    }//GEN-LAST:event_deleteTransButtonActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        
        contTrans.balanceActualPanelUpdate();
        contTrans.tableUpdateBalanceMonthlyWindow();
        setVisible(false);
        
    }//GEN-LAST:event_closeButtonActionPerformed

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        
        if (startDateChooser.getDate() != null) {
            startDate = getOptDateFromChooser(startDateChooser);
        } else {
            startDate = Optional.empty();
        }
        if (endDateChooser.getDate() != null) {
            endDate = getOptDateFromChooser(endDateChooser);
        } else {
            endDate = Optional.empty();
        }
        contTrans.tableUpdateCheckSelection(selectedCateg);
        setPeriodTextFields();
        
    }//GEN-LAST:event_okButtonActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addExpenseTransButton;
    private javax.swing.JButton addIncomeTransButton;
    private javax.swing.JButton allTransactionsButton;
    private javax.swing.JComboBox categoriesComboBox;
    private javax.swing.JButton closeButton;
    private javax.swing.JButton deleteTransButton;
    private com.toedter.calendar.JDateChooser endDateChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modifyTransButton;
    private javax.swing.JButton okButton;
    private javax.swing.JTextField periodBalanceTextField;
    private javax.swing.JTextField periodExpenseTextField;
    private javax.swing.JTextField periodIncomeTextField;
    private com.toedter.calendar.JDateChooser startDateChooser;
    private javax.swing.JTextField totalAmountCategoryTextField;
    private javax.swing.JTable transactionsTable;
    // End of variables declaration//GEN-END:variables

    
    public void categoriesComboBoxUpdate(List<Category> categList) {
        for (Category ctg : categList) {
            categoriesComboBox.addItem(ctg);
        }
    }
    
    
    public void tableUpdate(List<Transaction> transList) {
        dtm.getDataVector().clear();
        dtm.fireTableDataChanged();
        
        for (Transaction trans : transList) {
            Vector<Object> row = new Vector();
            row.add(trans.getCateg().getName());
            row.add(contTrans.getDf().format(trans.getAmount()));
            row.add(trans.getDate());
            dtm.addRow(row);
        }
        
//        for (Transaction trans : transList) {
//            List<Object> row = new ArrayList();
//            row.add(trans.getCateg().getName());
//            row.add(trans.getAmount());
//            row.add(trans.getDate());
//            dtm.addRow(row.toArray());
//        }
        
    }
    
    
    @Override
    public void tableChanged(TableModelEvent e) {
        int rowIndex = e.getFirstRow();
        int colIndex = e.getColumn();

        if (e.getType() == TableModelEvent.UPDATE && rowIndex >= 0 && colIndex >= 0) {
            newValue = dtm.getValueAt(rowIndex, colIndex);
            trans = contTrans.getTransList().get(rowIndex);

            switch (colIndex) {
                case 1:
                    modifyAmount();
                    break;
                case 2:
                    modifyDate();
                    break;
            }
            contTrans.tableUpdateCheckSelection(selectedCateg);
        }
    }
    
    
    public void centerHeadersDateColumn() {
        ((DefaultTableCellRenderer) transactionsTable
                .getTableHeader()
                .getDefaultRenderer())
                .setHorizontalAlignment(SwingConstants.CENTER);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        transactionsTable.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
    }
    
    
    public void alignTextFieldsRight() {
        totalAmountCategoryTextField.setHorizontalAlignment(SwingConstants.RIGHT);
        periodIncomeTextField.setHorizontalAlignment(SwingConstants.RIGHT);
        periodExpenseTextField.setHorizontalAlignment(SwingConstants.RIGHT);
        periodBalanceTextField.setHorizontalAlignment(SwingConstants.RIGHT);
    }
    
    
    public void modifyAmount() {
        if ((int) newValue > ControllerTransaction.ZERO) {
            setAndSaveAmount();
        } else {
            contTrans.equalsOrLessThanZeroMessage();
        }
    }
    
    
    public void setAndSaveAmount() {
        if (checkYesOption()) {
            setAmount();
            contTrans.saveTransaction(trans);
        }
    }
    
    
    public boolean checkYesOption() {
        return contTrans.modifyTransConfirmationMessage() == JOptionPane.YES_OPTION;
    }
    
    
    public void setAmount() {
        if (contTrans.checkEnumBeforeModifySelectedAmount() == CategoryEnum.INCOME) {
            trans.setAmount((int) newValue);
        } else {
            trans.setAmount((int) newValue * -1);
        }
    }
    
    
    public void modifyDate() {
        try {
            LocalDate date = LocalDate.parse(newValue + "", DateTimeFormatter.ISO_DATE);
            if (contTrans.modifyTransConfirmationMessage() == JOptionPane.YES_OPTION) {
                trans.setDate(date);
                contTrans.saveTransaction(trans);
            }
        } catch (DateTimeParseException ex) {
            contTrans.dateErrorMessageTableChanged();
        }
    }
    
    
    public String getTotalAmountOfSelectedCategory() {
        int totalAmount = 0;
        if (categoriesComboBox.getModel().getSelectedItem() != null
                && !categoriesComboBox.getModel().getSelectedItem().equals(comboBoxText)) {
            for (int i = 0; i < contTrans.getTransList().size(); i++) {
                totalAmount += contTrans.getTransList().get(i).getAmount();
            }
        }
        if (totalAmount == 0) {
            return "";
        }
        return contTrans.getDf().format(totalAmount);
    }
    
        
    public void setPeriodTextFields() {
        if (isCategory()) {
            periodIncomeTextField.setText("");
            periodExpenseTextField.setText("");
            periodBalanceTextField.setText("");
        } else {
            int periodIncome = makeEnumMap().getOrDefault(CategoryEnum.INCOME, 0);
            int periodExpense = makeEnumMap().getOrDefault(CategoryEnum.EXPENSE, 0);
            periodIncomeTextField.setText(contTrans.getDf().format(periodIncome));
            periodExpenseTextField.setText(contTrans.getDf().format(periodExpense * -1));
            int periodBalance = periodIncome + periodExpense;
            periodBalanceTextField.setText(contTrans.getDf().format(periodBalance));
        }
    }
    
    
    public boolean isCategory() {
        return categoriesComboBox.getModel().getSelectedItem() != null
                && !categoriesComboBox.getModel().getSelectedItem().equals(comboBoxText);
    }
    
    
    public EnumMap<CategoryEnum, Integer> makeEnumMap() {
        enumMap = new EnumMap<>(CategoryEnum.class);
        for (int i = 0; i < contTrans.getTransList().size(); i++) {
            Transaction trans = contTrans.getTransList().get(i);
            CategoryEnum ctgEnum = trans.getCateg().getCategEnum();
            enumMap.put(ctgEnum, enumMap.getOrDefault(ctgEnum, 0) + trans.getAmount());
        }
        return enumMap;
    }
    
    
    public JTable getTransactionsTable() {
        return transactionsTable;
    }

    public Optional<Category> getSelectedCateg() {
        return selectedCateg;
    }
        
    public void setTotalAmountCategoryTextField() {
        totalAmountCategoryTextField.setText(getTotalAmountOfSelectedCategory());
    }
    
    public Optional<LocalDate> getStartDate() {
        return startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return endDate;
    }
    
    public JComboBox getCategoriesComboBox() {
        return categoriesComboBox;
    }

    public String getComboBoxText() {
        return comboBoxText;
    }
    
    public Optional<LocalDate> getOptDateFromChooser(JDateChooser date) {
        return Optional.ofNullable(date.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }

}
