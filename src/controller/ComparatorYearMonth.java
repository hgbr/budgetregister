/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.YearMonth;
import java.util.Comparator;

/**
 *
 * @author
 */

public class ComparatorYearMonth implements Comparator<YearMonth> {
    
    @Override
    public int compare(YearMonth o1, YearMonth o2) {
        return o1.compareTo(o2);
    }

}
