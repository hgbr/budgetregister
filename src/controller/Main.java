/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.Main.mainFrame;
import gui.MainFrame;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author
 */
public class Main {

    public static String URL = "jdbc:mysql://localhost:3306/budgetregister?serverTimezone=CET";
    public static String USER = "root";
    public static String PASSWORD = "1234";
    
    public static Connection conn;
    public static MainFrame mainFrame;
    public static ControllerCategoryIncome incCategCont;
    public static ControllerCategoryExpense expCategCont;
    public static ControllerTransaction transactionCont;
    
    public static int balanceAmount;
    
    
    public static void main(String[] args) {
                
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(gui.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(gui.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(gui.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(gui.MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException ex){
            dataBaseErrorMessage(ex);
            System.exit(0);
        }
        
        incCategCont = new ControllerCategoryIncome(conn);
        expCategCont = new ControllerCategoryExpense(conn);
        transactionCont = new ControllerTransaction(conn);
        
        balanceAmount = transactionCont.getBalanceAmount();
        mainFrame = new MainFrame();
        
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                mainFrame.setVisible(true);
                mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                        int reply = windowClosingConfirmationMessage();
                        if (reply == JOptionPane.YES_OPTION) {
                            closeDatabase();
                            System.exit(0);
                        } else {
                            mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                        }
                    }
                });
            }
        });
        
    }
    
        
    /**
     * Brings up a database error message dialog titled "Adatbázis hiba".
     * @param ex Exception ex
     * @return void
     * @throws nothing
     * @see
     */
    public static void dataBaseErrorMessage(Exception ex) {
        JOptionPane.showMessageDialog(null, ex, "Adatbázis hiba", JOptionPane.ERROR_MESSAGE);
    }
    
    
    
    // METHODS OF EXIT MENU ITEM
        
    
    public static void exitMenuItem() {
        int reply = windowClosingConfirmationMessage();
        if (reply == JOptionPane.YES_OPTION) {
            closeDatabase();
            System.exit(0);
        }
    }
    
    public static int windowClosingConfirmationMessage() {
        ImageIcon icon = new ImageIcon("questionIcon.jpg");
        String[] options = {"Igen", "Nem"};
        int reply = JOptionPane.showOptionDialog(
                mainFrame,
                "Biztosan ki akarsz lépni a programból?",
                "Program bezárása",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, icon, options, null);
        return reply;
    }
    
    public static void closeDatabase() {
        incCategCont.closePreparedStatements();
        expCategCont.closePreparedStatements();
        transactionCont.closePreparedStatements();
        closeConnection();
    }
    
    public static void closeConnection() {
        try {
            if (Main.conn != null) {
                Main.conn.close();
            }
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }
    
}
