/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.Main.dataBaseErrorMessage;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import model.entity.Category;


/**
 * ControllerCategory class implements or declares all methods that required in all extensions of ControllerCategory.
 * ControllerCategory declares the methods that create new windows.
 * Its methods define CRUD operations.
 * @param
 * @return
 * @throws
 * @see
 */
public abstract class ControllerCategory extends Controller {

    protected Category selected;
    

    public ControllerCategory(Connection conn) {
        super(conn);
    }
    
    
    
// METHODS OF CATEGORIES
    
    
    public abstract void openEditCategoryDialog();
    public abstract void openAddNewCategoryDialog();
    public abstract void openModifyCategoryDialog();
    public abstract void openDeleteCategoryDialog();
    public abstract void listUpdate(List<Category> categ);
    
    
    /**
     * Confirm the selected category before deleting.
     * @param selected the selected Category object.
     * @return void
     * @throws
     * @see
     */
    public void confirmSelectedCategory(Category selected) {
        if (deleteConfirmationMessage() == JOptionPane.YES_OPTION) {
            deleteCategory(selected);
        }
    }
    
    
    /**
     * Returns a list of all items, in increasing order based on their name.
     * @param
     * @return a list of all items
     * @throws
     * @see
     */
    public List<Category> getAllCategoriesByTypeAscend() {
//        categList = getAllCategoriesByType();
        getAllCategoriesByType();
        categList.sort(new ComparatorNameCateg());
        return categList;
    }
    
    /**
     * Returns a list of all items, in decreasing order based on their name.
     * @param
     * @return a list of all items
     * @throws
     * @see
     */
    public List<Category> getAllCategoriesByTypeDescend() {
        getAllCategoriesByType();
        categList.sort(new ComparatorNameCateg().reversed());
        return categList;
    }

    
    /**
     * Calls the getAllCategoriesByType method of CategoryDAO class.
     * @param
     * @return a list of all items by type
     * @throws
     * @see
     */
    public List<Category> getAllCategoriesByType(){
        try {
            categList = categoryDAO.getAllCategoriesByType(ctgEnum);
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return categList;
    }
    
    
    /**
     * Calls the save method of CategoryDAO and updates the jList.
     * @param ctg a Category object
     * @return
     * @throws
     * @see
     */
    public void saveCategory(Category ctg) {
        try {
            categoryDAO.save(ctg);
            listUpdate(getAllCategoriesByTypeAscend());
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }
    
    
    /**
     * Calls the getCategoryByName method of CategoryDAO.
     * @param name the name of the Category to be searched
     * @return a Category object or null if there is no category with this name
     * @throws
     * @see
     */
    public Category getCategoryByName(String name) {
        Category categ = new Category();
        try {
            categ = categoryDAO.getCategoryByName(name, ctgEnum);
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return categ;
    }
    
    
    /**
     * Calls the getCategoryById method of CategoryDAO.
     * @param id the id of the Category to be searched
     * @return a Category object or null if there is no category with this id
     * @throws
     * @see
     */
    public Category getCategoryById(int id) {
        Category categ = new Category();
        try {
            categ = categoryDAO.getCategoryById(id);
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return categ;
    }
    
    
    /**
     * Calls the delete method of CategoryDAO and updates the jList.
     * @param ctg a Category object
     * @return
     * @throws
     * @see
     */
    public void deleteCategory(Category ctg) {
        try {
            categoryDAO.delete(ctg.getId());
            listUpdate(getAllCategoriesByTypeAscend());
        } catch (SQLIntegrityConstraintViolationException ex) {
            SqlIcvMessage(ex);
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }
    
    
    
    
    
    
    // METHODS OF MESSAGES
           
    
    
    /**
     * Brings up an information message dialog titled "Információ" and displays following message:
     * "A(z) 'kiválasztott' kategóriához tranzakció van rögzítve, ezért nem törölhető!"
     * @param ex SQLIntegrityConstraintViolationException
     * @return
     * @throws
     * @see
     */
    public void SqlIcvMessage(SQLIntegrityConstraintViolationException ex) {
        JOptionPane.showMessageDialog(dialog,
                "A(z)  \"" + selected + "\"  kategóriához tranzakció van rögzítve, ezért nem törölhető!",
                "Információ",
                JOptionPane.INFORMATION_MESSAGE);
    }
    
    
    /**
     * Brings up an error message dialog titled "Duplikáció hiba" and displays following message:
     * "Ez a kategória már létezik, adj meg más kategória nevet!"
     * @param
     * @return
     * @throws
     * @see
     */
    public void categoryExistsMessage() {
        JOptionPane.showMessageDialog(dialog,
                "Ez a kategória már létezik, adj meg más kategória nevet!",
                "Duplikáció hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    @Override
    public int deleteConfirmationMessage() {
        ImageIcon icon = new ImageIcon("questionIcon.jpg");
        String[] options = {"Igen", "Nem"};
        int reply = JOptionPane.showOptionDialog(dialog,
                "Biztosan törölni szeretnéd a kiválasztott  \"" + selected + "\"  kategóriát?",
                "Kategória törlése",
                JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, null);
        return reply;
    }

    @Override
    public void notSelectedMessage() {
        JOptionPane.showMessageDialog(dialog,
                "Válassz ki egy kategóriát!",
                "Kiválasztás hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    /**
     * Brings up an error message dialog titled
     * "Hiányzó név hiba"
     * and displays following message:
     * "A név nem lehet üres, adj meg egy kategória nevet!"
     * @param
     * @return void
     * @throws nothing
     * @see
     */
    @Override
    public void categoryNameIsMissingMessage() {
        JOptionPane.showMessageDialog(dialog,
                "A név nem lehet üres, adj meg egy kategória nevet!",
                "Hiányzó név hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
}
