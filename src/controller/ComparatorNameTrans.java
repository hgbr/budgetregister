/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Comparator;
import model.entity.Transaction;

/**
 *
 * @author
 */

public class ComparatorNameTrans implements Comparator<Transaction> {

    @Override
    public int compare(Transaction o1, Transaction o2) {
        return o1.getCateg().getName().compareToIgnoreCase(o2.getCateg().getName());
    }

}
