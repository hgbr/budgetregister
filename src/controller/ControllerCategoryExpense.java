/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.EditExpenseDialog;
import gui.EditExpenseNewModifyDialog;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import model.entity.Category;
import model.entity.CategoryEnum;


/**
 * ControllerCategoryExpense class defines methods that create new windows to manage expense categories.
 * @param
 * @return
 * @throws
 * @see
 */
public class ControllerCategoryExpense extends ControllerCategory {
    
    private EditExpenseDialog editExpDialog;
    
    
    public ControllerCategoryExpense(Connection conn) {
        super(conn);
    }
    
    
    /**
     * Opens a new expense dialog window.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openEditCategoryDialog() {
        ctgEnum = CategoryEnum.EXPENSE;
        editExpDialog = new EditExpenseDialog(Main.mainFrame, this);
        dialog = editExpDialog;
        editExpDialog.setVisible(true);
    }
    
    
    /**
     * Opens a new dialog window for add a new expense category.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openAddNewCategoryDialog() {
        EditExpenseNewModifyDialog editExpNMD = new EditExpenseNewModifyDialog(editExpDialog, this, Optional.empty());
        editExpNMD.setVisible(true);
    }
    
    
    /**
     * Opens a new dialog window for modify the selected expense category.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openModifyCategoryDialog() {
        selected = (Category) editExpDialog.getjListExpense().getSelectedValue();
        if (selected != null) {
            EditExpenseNewModifyDialog editExpNMD = new EditExpenseNewModifyDialog(editExpDialog, this, Optional.ofNullable(selected));
            editExpNMD.setVisible(true);
        } else {
            notSelectedMessage();
        }
    }
    
    
    /**
     * Opens a new dialog window for delete the selected expense category.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openDeleteCategoryDialog() {
        selected = (Category) editExpDialog.getjListExpense().getSelectedValue();
        if (selected != null) {
            confirmSelectedCategory(selected);
        } else {
            notSelectedMessage();
        }
    }
    
    
    /**
     * Updates the list in expense categories dialog window.
     * @param categ list of expense category objects
     * @return
     * @throws
     * @see
     */
    public void listUpdate(List<Category> categ) {
        editExpDialog.jListExpenseUpdate(categ);
    }
    
    
}
