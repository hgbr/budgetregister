/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.Main.dataBaseErrorMessage;
import gui.TransMngrNewModifyDialog;
import gui.TransactionManagerDialog;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.entity.Category;
import model.entity.CategoryEnum;
import model.entity.Transaction;
import org.apache.commons.lang3.ArrayUtils;


/**
 * ControllerTransaction class defines methods that create new windows to manage transactions.
 * @param
 * @return
 * @throws
 * @see
 */
public class ControllerTransaction extends Controller {
    
    public static final int ZERO = 0;
    
    private TransactionManagerDialog transMngrDialog;
    private List<Transaction> transList;
    private int[] indicesOfSelectedRows;
    private Transaction selectedTrans;
    private TransMngrNewModifyDialog transManagerNMD;
    private DecimalFormat df;
    
    
    public ControllerTransaction(Connection conn) {
        super(conn);
        df = new DecimalFormat("#,###,###,###");
    }
    
    
    /**
     * Opens a new transaction dialog window.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openTransactionManagerDialog() {
        transMngrDialog = new TransactionManagerDialog(Main.mainFrame, this);
        dialog = transMngrDialog;
        transMngrDialogAddWindowListener();
        transMngrDialog.setVisible(true);
    }
    
    
    /**
     * Adds a window listener to the transaction manager dialog
     * and calls balanceActualPanelUpdate() and tableUpdateBalanceMonthlyWindow() methods.
     * @param
     * @return
     * @throws
     * @see
     */
    public void transMngrDialogAddWindowListener() {
        transMngrDialog.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                balanceActualPanelUpdate();
                tableUpdateBalanceMonthlyWindow();
            }
        });
    }
    
    
    /**
     * Updates actual balance in the BalanceActualPanel.
     * @param
     * @return
     * @throws
     * @see
     */
    public void balanceActualPanelUpdate() {
        Main.mainFrame.getBalanceActualPanel().displayBalance(df.format(getBalanceAmount()) + " Ft");
    }
    
    
    /**
     * Calls tableUpdateTransMngrDialog method and
     * calls setTotalAmountCategoryTextField() method from the TransactionManagerDialog class. 
     * @param selected the selected Category object
     * @return
     * @throws
     * @see
     */
    public void tableUpdateCheckSelection(Optional<Category> selected) {
        tableUpdateTransMngrDialog(getAllTransByCategoryAndDate(selected, transMngrDialog.getStartDate(), transMngrDialog.getEndDate()));
        transMngrDialog.setTotalAmountCategoryTextField();
        transMngrDialog.setPeriodTextFields();
    }
    
    
    /**
     * Updates the jTable with the given list in the TransactionManagerDialog class.
     * @param transList list to update the jTable
     * @return
     * @throws
     * @see
     */
    public void tableUpdateTransMngrDialog(List<Transaction> transList) {
        DefaultTableModel dtm = (DefaultTableModel) transMngrDialog.getTransactionsTable().getModel();
        dtm.getDataVector().clear();
        dtm.fireTableDataChanged();
        
        for (Transaction trans : transList) {
            Vector<Object> row = new Vector();
            row.add(trans.getCateg().getName());
            row.add(df.format(trans.getAmount()));
            row.add(trans.getDate());
            dtm.addRow(row);
        }
    }
    
    
    /**
     * Calls getAllTransactions() and sortTransList() methods.
     * @param
     * @return a list of Transactions in descending order by date
     * @throws
     * @see
     */
    public List<Transaction> getAllTransactionsDescByDate() {
        getAllTransactions();
        sortTransList();
        return transList;
    }
    
    
    /**
     * Calls getAllTransactions() method of TransactionDAO class.
     * @param
     * @return a list of Transactions
     * @throws
     * @see
     */
    public List<Transaction> getAllTransactions() {
        try {
            transList = transactionDAO.getAllTransactions();
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return transList;
    }
    
    
    /**
     * Calls the save method of TransactionDAO class.
     * @param trans a Transaction object
     * @return
     * @throws
     * @see
     */
    public void saveTransaction(Transaction trans) {
        try {
            transactionDAO.save(trans);
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }
    
    
    /**
     * Checks is there a selected row and calls deleteSelectedTransaction method.
     * @param
     * @return
     * @throws
     * @see
     */
    public void deleteButtonTransMngrDialog() {
        indicesOfSelectedRows = transMngrDialog.getTransactionsTable().getSelectedRows();
        if (!ArrayUtils.isEmpty(indicesOfSelectedRows)) {
            if (deleteConfirmationMessage() == JOptionPane.YES_OPTION) {
                deleteSelectedTransaction(indicesOfSelectedRows);
            }
        } else {
            notSelectedMessage();
        }
        indicesOfSelectedRows = null;
    }
    
    
    /**
     * Gets an array with the selected rows and calls deleteTransaction method on each element of the array.
     * @param indicesOfSelectedRows an array containing the selected rows
     * @return
     * @throws
     * @see
     */
    public void deleteSelectedTransaction(int[] indicesOfSelectedRows) {
        List<Transaction> tmpList = new ArrayList<>(transList);
        for (int i = 0; i < indicesOfSelectedRows.length; i++) {
            int selectedRowIndex = indicesOfSelectedRows[i];
            selectedTrans = transList.get(selectedRowIndex);
            deleteTransaction(selectedTrans);
            tmpList.remove(selectedTrans);
        }
        transList = tmpList;
    }
    
    
    /**
     * Calls the delete method of TransactionDAO class.
     * @param trans a Transaction object
     * @return
     * @throws
     * @see
     */
    public void deleteTransaction(Transaction trans) {
        try {
            transactionDAO.delete(trans.getId());
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }

    
    /**
     * Opens an add new income transaction dialog window.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openTransMngrAddNewIncDialog() {
        transManagerNMD = new TransMngrNewModifyDialog(transMngrDialog, this,
                Optional.ofNullable(CategoryEnum.INCOME));
        dialog = transManagerNMD;
        transManagerNMD.setVisible(true);
    }
    
    
    /**
     * Opens an add new expense transaction dialog window.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openTransMngrAddNewExpDialog() {
        transManagerNMD = new TransMngrNewModifyDialog(transMngrDialog, this,
                Optional.ofNullable(CategoryEnum.EXPENSE));
        dialog = transManagerNMD;
        transManagerNMD.setVisible(true);
    }
    
    
    /**
     * Opens modify transaction dialog window.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openTransMngrModifyDialog() {
        indicesOfSelectedRows = transMngrDialog.getTransactionsTable().getSelectedRows();
        if (!ArrayUtils.isEmpty(indicesOfSelectedRows)) {
            if (indicesOfSelectedRows.length == 1) {
                int SelectedRowIndex = indicesOfSelectedRows[0];
                selectedTrans = transList.get(SelectedRowIndex);
                transManagerNMD = new TransMngrNewModifyDialog(transMngrDialog, this, Optional.empty());
                dialog = transManagerNMD;
                transManagerNMD.setVisible(true);
                tableUpdateTransMngrDialog(transList);
            } else {
                selectOnlyOneTransactionMessage();
            }
        } else {
            notSelectedMessage();
        }
        indicesOfSelectedRows = null;
    }
    
    
    /**
     * Checks enum of the selected row in the jTable of the TransactionManagerDialog class.
     * @param
     * @return an enum from CategoryEnum class
     * @throws
     * @see
     */
    public CategoryEnum checkEnumBeforeModifySelectedAmount() {
        int SelectedRowIndex = transMngrDialog.getTransactionsTable().getSelectedRow();
        selectedTrans = transList.get(SelectedRowIndex);
        ctgEnum = selectedTrans.getCateg().getCategEnum();
        return ctgEnum;
    }
    
    
    /**
     * Calls the getBalanceAmount method of TransactionDAO class.
     * @param
     * @return amount of the balance
     * @throws
     * @see
     */
    public int getBalanceAmount() {
        int balanceAmount = 0;
        try {
            balanceAmount = transactionDAO.getBalanceAmount();
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return balanceAmount;
    }
    
    
    /**
     * Checks is there a selected Category object and
 calls getAllTransByDate or getAllTransByCategory methods and then
 calls sortTransList method.
     * @param categ the selected Category object as an Optional
     * @param start the start date as an Optional LocalDate object
     * @param end the end date as an Optional LocalDate object
     * @return a list of transactions
     * @throws
     * @see
     */
    public List<Transaction> getAllTransByCategoryAndDate(Optional<Category> categ, Optional<LocalDate> start, Optional<LocalDate> end) {
        if (!categ.isPresent()) {
            getAllTransByDate(start, end);
        } else {
            getAllTransByCategory(categ, start, end);
        }
        sortTransList();
        return transList;
    }
    
    
    /**
     * Calls the getAllTransByDate method of TransactionDAO class.
     * @param start the start date as an Optional LocalDate object
     * @param end the end date as an Optional LocalDate object
     * @return a list of transactions
     * @throws
     * @see
     */
    public List<Transaction> getAllTransByDate(Optional<LocalDate> start, Optional<LocalDate> end) {
        try {
            transList = transactionDAO.getAllTransByDate(checkStartDate(start), checkEndDate(end));
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return transList;
    }
    
    
    /**
     * Calls the getAllTransByCategory method of TransactionDAO class.
     * @param categ the selected Category object as an Optional
     * @param start the start date as an Optional LocalDate object
     * @param end the end date as an Optional LocalDate object
     * @return a list of transactions
     * @throws
     * @see
     */
    public List<Transaction> getAllTransByCategory(Optional<Category> categ, Optional<LocalDate> start, Optional<LocalDate> end) {
        try {
            transList = transactionDAO.getAllTransByCategory(categ, checkStartDate(start), checkEndDate(end));
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return transList;
    }
    
    
    /**
     * Checks is there a start date, if there is no start date sets and returns the default start date.
     * @param startDate the start date as an Optional LocalDate object
     * @return a start date as an Optional LocalDate object
     * @throws
     * @see
     */
    public Optional<LocalDate> checkStartDate(Optional<LocalDate> startDate) {
        if (!startDate.isPresent()) {
            startDate = Optional.ofNullable(LocalDate.parse("0001-01-01"));
        }
        return startDate;
    }
    
    
    /**
     * Checks is there an end date, if there is no end date sets and returns the default end date.
     * @param endDate the end date as an Optional LocalDate object
     * @return an end date as an Optional LocalDate object
     * @throws
     * @see
     */
    public Optional<LocalDate> checkEndDate(Optional<LocalDate> endDate) {
        if (!endDate.isPresent()) {
            endDate = Optional.ofNullable(LocalDate.parse("9999-01-01"));
        }
        return endDate;
    }
    
    
    /**
     * Updates monthly balance in the BalanceMonthlyPanel.
     * @param
     * @return
     * @throws
     * @see
     */
    public void tableUpdateBalanceMonthlyWindow() {
        Main.mainFrame.getBalanceMonthlyPanel().tableUpdate(Main.mainFrame.getBalanceMonthlyPanel().monthlyDataList(getAllTransactions()));
    }
    
    
    /**
     * Sorting the transList field in descending order by date or
     * in ascending order by name if the dates are equal.
     * @param
     * @return
     * @throws
     * @see
     */
    public void sortTransList() {
        transList.sort(new ComparatorComplex(new ComparatorDate().reversed(), new ComparatorNameTrans()));
    }
    
    
    
    
    
    
    
    
    /**
     * Brings up an error message dialog titled
     * "Hiányzó kategória hiba"
     * and displays following message:
     * "A kategória nem lehet üres, válassz egy kategóriát!"
     * @param
     * @return void
     * @throws nothing
     * @see
     */
    @Override
    public void categoryNameIsMissingMessage() {
        JOptionPane.showMessageDialog(dialog,
                "A kategória nem lehet üres, válassz egy kategóriát!",
                "Hiányzó kategória hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    @Override
    public int deleteConfirmationMessage() {
        ImageIcon icon = new ImageIcon("questionIcon.jpg");
        String[] options = {"Igen", "Nem"};
        int reply = JOptionPane.showOptionDialog(dialog,
                "Biztosan törölni szeretnéd a kiválasztott tranzakció(ka)t?",
                "Tranzakció(k) törlése",
                JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, null);
        return reply;
    }
    
    
    @Override
    public void notSelectedMessage() {
        JOptionPane.showMessageDialog(dialog,
                "Válassz ki tranzakciót!",
                "Kiválasztás hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    public void selectOnlyOneTransactionMessage() {
        JOptionPane.showMessageDialog(dialog,
                "Módosításhoz csak egy tranzakció választható!",
                "Kiválasztás hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    public void numberFormatExMessage() {
        JOptionPane.showMessageDialog(dialog,
                "Adj meg egy összeget!",
                "Összeg hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    public void equalsOrLessThanZeroMessage() {
        JOptionPane.showMessageDialog(dialog,
                "Nullánál nagyobb egész számot adj meg!",
                "Összeg hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    public void dateErrorMessageTableChanged() {
        JOptionPane.showMessageDialog(dialog,
                "A dátumot ebben a formátumban add meg:\n"
                + ">>  éééé-hh-nn  <<",
                "Dátum hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    public void dateErrorMessageDialog() {
        JOptionPane.showMessageDialog(dialog,
                "Adj meg egy dátumot ebben a formátumban:\n"
                + ">>  éééé-hh-nn  <<",
                "Dátum hiba",
                JOptionPane.ERROR_MESSAGE);
    }
    
    
    public int modifyTransConfirmationMessage() {
        ImageIcon icon = new ImageIcon("questionIcon.jpg");
        String[] options = {"Igen", "Nem"};
        int reply = JOptionPane.showOptionDialog(dialog,
                "Biztosan módosítani szeretnéd?",
                "Tranzakció módosítása",
                JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, null);
        return reply;
    }

    
    
    
    
    public TransactionManagerDialog getTransMngrDialog() {
        return transMngrDialog;
    }

    public List<Transaction> getTransList() {
        return transList;
    }

    public int[] getIndicesOfSelectedRows() {
        return indicesOfSelectedRows;
    }

    public Transaction getSelectedTrans() {
        return selectedTrans;
    }

    public DecimalFormat getDf() {
        return df;
    }
    
    
}
