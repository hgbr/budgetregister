/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import gui.EditIncomeDialog;
import gui.EditIncomeNewModifyDialog;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import model.entity.Category;
import model.entity.CategoryEnum;


/**
 * ControllerCategoryIncome class defines methods that create new windows to manage income categories.
 * @param
 * @return
 * @throws
 * @see
 */
public class ControllerCategoryIncome extends ControllerCategory {
    
    private EditIncomeDialog editIncDialog;
    

    public ControllerCategoryIncome(Connection conn) {
        super(conn);
    }
    
    
    /**
     * Opens a new income dialog window.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openEditCategoryDialog() {
        ctgEnum = CategoryEnum.INCOME;
        editIncDialog = new EditIncomeDialog(Main.mainFrame, this);
        dialog = editIncDialog;
        editIncDialog.setVisible(true);
    }
    
    
    /**
     * Opens a new dialog window for add a new income category.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openAddNewCategoryDialog() {
        EditIncomeNewModifyDialog editIncNMD = new EditIncomeNewModifyDialog(editIncDialog, this, Optional.empty());
        editIncNMD.setVisible(true);
    }
    
    
    /**
     * Opens a new dialog window for modify the selected income category.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openModifyCategoryDialog() {
        selected = (Category) editIncDialog.getjListIncome().getSelectedValue();
        if (selected != null) {
            EditIncomeNewModifyDialog editIncNMD = new EditIncomeNewModifyDialog(editIncDialog, this, Optional.ofNullable(selected));
            editIncNMD.setVisible(true);
        } else {
            notSelectedMessage();
        }
    }
    
    
    /**
     * Opens a new dialog window for delete the selected income category.
     * @param
     * @return
     * @throws
     * @see
     */
    public void openDeleteCategoryDialog() {
        selected = (Category) editIncDialog.getjListIncome().getSelectedValue();
        if (selected != null) {
            confirmSelectedCategory(selected);
        } else {
            notSelectedMessage();
        }
    }
    
    
    /**
     * Updates the list in income categories window.
     * @param categ list of income category objects
     * @return
     * @throws
     * @see
     */
    public void listUpdate(List<Category> categ) {
        editIncDialog.jListIncomeUpdate(categ);
    }
    
    
}
