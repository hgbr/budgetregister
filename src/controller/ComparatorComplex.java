/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Comparator;

/**
 *
 * @author
 */
public class ComparatorComplex implements Comparator {
    
    Comparator firstComp;
    Comparator secondComp;

    
    public ComparatorComplex(Comparator firstComp, Comparator secondComp) {
        this.firstComp = firstComp;
        this.secondComp = secondComp;
    }

    
    @Override
    public int compare(Object o1, Object o2) {
        int first = firstComp.compare(o1, o2);
        int second = secondComp.compare(o1, o2);
        if (first == 0) {
            return second;
        }
        return first;
    }
    
}
