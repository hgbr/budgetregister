/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Comparator;
import model.entity.MonthlyData;

/**
 *
 * @author
 */

public class ComparatorMonthlyData implements Comparator<MonthlyData> {
    
    @Override
    public int compare(MonthlyData o1, MonthlyData o2) {
        return o1.getDate().compareTo(o2.getDate());
    }

}
