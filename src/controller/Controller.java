/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.Main.dataBaseErrorMessage;
import java.util.List;
import model.entity.Category;
import model.entity.CategoryEnum;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JDialog;
import model.dao.CategoryDAO;
import model.dao.TransactionDAO;
import model.dao.impl.CategoryDAOJDBCImpl;
import model.dao.impl.TransactionDAOJDBCImpl;


/**
 * Controller class receives a Connection object from the Main class and creates DAOs
 * in its own constructor.
 * Controller class implements or declares all methods that required in all extensions of Controller.
 * Defines two methods to query all categories.
 * @author
 * @param
 * @return void
 * @throws nothing
 * @see
 */
public abstract class Controller {
    
//    protected Connection conn;
    
    protected JDialog dialog;
    protected CategoryEnum ctgEnum;
    protected CategoryDAO categoryDAO;
    protected TransactionDAO transactionDAO;
    protected List<Category> categList;
    
    
    public Controller(Connection conn) {
//        this.conn = conn;
        try {
            categoryDAO = new CategoryDAOJDBCImpl(conn);
            transactionDAO = new TransactionDAOJDBCImpl(conn, categoryDAO);
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }
    
   
    
    
// METHODS OF CATEGORIES
        
    
    /**
     * Returns all categories sorted by name.
     * @param
     * @return a list of categories sorted by name.
     * @throws nothing
     * @see
     */
    public List<Category> getAllCategoriesBySort() {
        getAllCategories();
        categList.sort(new ComparatorNameCateg());
        return categList;
    }
    
    /**
     * Returns all categories.
     * @param
     * @return a list of categories.
     * @throws nothing
     * @see
     */
    public List<Category> getAllCategories(){
        try {
            categList = categoryDAO.getAllCategories();
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
        return categList;
    }
    
    
    
    
// METHODS OF CATEGORIES AND TRANSACTIONS
    
    
    public void closePreparedStatements() {
        try {
            if (categoryDAO != null) {
                categoryDAO.close();
            }
            if (transactionDAO != null) {
                transactionDAO.close();
            }
        } catch (SQLException ex) {
            dataBaseErrorMessage(ex);
        }
    }
    
    
    
    
// METHODS OF MESSAGES
           

    public abstract void notSelectedMessage();
    public abstract void categoryNameIsMissingMessage();
    public abstract int deleteConfirmationMessage();
    
    
    
    
    
// GETTERS
    
    
    public CategoryEnum getCtgEnum() {
        return ctgEnum;
    }
    
    public List<Category> getCategList() {
        return categList;
    }
    
    
}
