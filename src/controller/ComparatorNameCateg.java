/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Comparator;
import model.entity.Category;

/**
 *
 * @author
 */

public class ComparatorNameCateg implements Comparator<Category> {

    @Override
    public int compare(Category o1, Category o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
    }

}
