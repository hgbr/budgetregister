/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import model.dao.CategoryDAO;
import model.dao.TransactionDAO;
import model.entity.Category;
import model.entity.Transaction;

/**
 *
 * @author
 */
public class TransactionDAOJDBCImpl implements TransactionDAO {
    
//    private Controller cont;
    private CategoryDAO categDao;
    private Map<Integer, Category> categMap;
    private ResultSet generatedKeys;
    
    private PreparedStatement getAllTransactions;
    private PreparedStatement getAllTransByCategoryAndDate;
    private PreparedStatement getAllTransByDate;
    private PreparedStatement getBalanceAmount;
    private PreparedStatement insert;
    private PreparedStatement update;
    private PreparedStatement delete;

    
    public TransactionDAOJDBCImpl(Connection conn, CategoryDAO categDao) throws SQLException {
        this.categDao = categDao;
        getAllTransactions = conn.prepareStatement("SELECT * FROM transaction");
        getAllTransByCategoryAndDate = conn.prepareStatement(
                "SELECT * FROM transaction WHERE category_id = ? AND date BETWEEN ? AND ?");
        getAllTransByDate = conn.prepareStatement("SELECT * FROM transaction WHERE date BETWEEN ? AND ?");
        getBalanceAmount = conn.prepareStatement("SELECT SUM(amount) AS amount FROM transaction");
        insert = conn.prepareStatement("INSERT INTO transaction (category_id, amount, date) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
        update = conn.prepareStatement(
                "UPDATE transaction SET category_id = ?, amount = ?, date = ? WHERE id = ?");
        delete = conn.prepareStatement("DELETE FROM transaction WHERE id = ?");
    }

    
    @Override
    public List<Transaction> getAllTransactions() throws SQLException {
        ResultSet rs = getAllTransactions.executeQuery();
        List<Transaction> transList = makeList(rs);
        rs.close();
        return transList;
    }

    @Override
    public List<Transaction> getAllTransByCategory(Optional<Category> categ, Optional<LocalDate> start, Optional<LocalDate> end) throws SQLException {
        getAllTransByCategoryAndDate.setInt(1, categ.get().getId());
        getAllTransByCategoryAndDate.setDate(2, Date.valueOf(start.get()));
        getAllTransByCategoryAndDate.setDate(3, Date.valueOf(end.get()));
        ResultSet rs = getAllTransByCategoryAndDate.executeQuery();
        List<Transaction> transList = makeList(rs);
        rs.close();
        return transList;
    }
    
    @Override
    public List<Transaction> getAllTransByDate(Optional<LocalDate> start, Optional<LocalDate> end) throws SQLException {
        getAllTransByDate.setDate(1, Date.valueOf(start.get()));
        getAllTransByDate.setDate(2, Date.valueOf(end.get()));
        ResultSet rs = getAllTransByDate.executeQuery();
        List<Transaction> transList = makeList(rs);
        rs.close();
        return transList;
    }
    
    @Override
    public int getBalanceAmount() throws SQLException {
        ResultSet rs = getBalanceAmount.executeQuery();
        Transaction trans = makeAmount(rs);
        rs.close();
        return trans.getAmount();
    }
    
    @Override
    public int insert(Transaction trans) throws SQLException {
        insert.setInt(1, trans.getCateg().getId());
        insert.setInt(2, trans.getAmount());
        insert.setDate(3, Date.valueOf(trans.getDate()));
        return insert.executeUpdate();
    }
    
    
//    public Transaction insert(Transaction trans) throws SQLException {
//        insert.setInt(1, trans.getCateg().getId());
//        insert.setInt(2, trans.getAmount());
//        insert.setDate(3, Date.valueOf(trans.getDate()));
//        insert.executeUpdate();
//        generatedKeys = insert.getGeneratedKeys();
//        if (generatedKeys.next()) {
//            int insertedID = generatedKeys.getInt(1);
//            trans.setId(insertedID);
//        }
//        generatedKeys.close();
//        return trans;
//    }
    
    
    @Override
    public int update(Transaction trans) throws SQLException {
        update.setInt(1, trans.getCateg().getId());
        update.setInt(2, trans.getAmount());
        update.setDate(3, Date.valueOf(trans.getDate()));
        update.setInt(4, trans.getId());
        return update.executeUpdate();
    }

    @Override
    public void save(Transaction trans) throws SQLException {
        if (trans.getId() == null) {
            insert(trans);
        } else {
            update(trans);
        }
    }

    @Override
    public int delete(int id) throws SQLException {
        delete.setInt(1, id);
        return delete.executeUpdate();
    }

    @Override
    public void close() throws SQLException {
        getAllTransactions.close();
        getAllTransByCategoryAndDate.close();
        getAllTransByDate.close();
        getBalanceAmount.close();
        insert.close();
        update.close();
        delete.close();
    }
    
    private List<Transaction> makeList(ResultSet rs) throws SQLException {
        categMap = categDao.makeCategoryMap();
        List<Transaction> transList = new ArrayList<>();
        while (rs.next()) {
            transList.add(makeOne(rs));
        }
        return transList;
    }

    private Transaction makeOne(ResultSet rs) throws SQLException {
        Transaction trans = new Transaction();
        trans.setId(rs.getInt("id"));
        trans.setCateg(categMap.get(rs.getInt("category_id")));
        trans.setAmount(rs.getInt("amount"));
        trans.setDate(rs.getDate("date").toLocalDate());
        return trans;
    }
    
    private Transaction makeAmount(ResultSet rs) throws SQLException {
        Transaction trans = new Transaction();
        if (rs.next()) {
            trans.setAmount(rs.getInt("amount"));
        }
        return trans;
    }
    
}
