/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao.impl;

import model.dao.CategoryDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.entity.Category;
import model.entity.CategoryEnum;
import model.CategoryTypeAware;


/**
 *
 * @author
 */

public class CategoryDAOJDBCImpl implements CategoryDAO, CategoryTypeAware {
    
    private PreparedStatement getAllCategories;
    private PreparedStatement getAllCategoriesByType;
    private PreparedStatement getCategoryById;
    private PreparedStatement getCategoryByName;
    private PreparedStatement insert;
    private PreparedStatement update;
    private PreparedStatement delete;
      
    
    public CategoryDAOJDBCImpl(Connection conn) throws SQLException {
        this.getAllCategories = conn.prepareStatement("SELECT * FROM category");
        this.getAllCategoriesByType = conn.prepareStatement("SELECT * FROM category WHERE type = ?");
        this.getCategoryById = conn.prepareStatement("SELECT * FROM category WHERE id = ?");
        this.getCategoryByName = conn.prepareStatement("SELECT * FROM category WHERE name = ? AND type = ?");
        this.insert = conn.prepareStatement("INSERT INTO category (name, type) VALUES (?,?)");
        this.update = conn.prepareStatement("UPDATE category SET name = ? WHERE id = ?");
        this.delete = conn.prepareStatement("DELETE FROM category WHERE id = ?");
    }
    
    @Override
    public List<Category> getAllCategories() throws SQLException {
        ResultSet rs = getAllCategories.executeQuery();
        List<Category> ctgList = makeList(rs);
        rs.close();
        return ctgList;
    }

    @Override
    public List<Category> getAllCategoriesByType(CategoryEnum ctgEnum) throws SQLException {
        getAllCategoriesByType.setInt(1, ctgEnum.getType());
        ResultSet rs = getAllCategoriesByType.executeQuery();
        List<Category> ctgList = makeList(rs);
        rs.close();
        return ctgList;
    }

    @Override
    public Category getCategoryById(int id) throws SQLException {
        getCategoryById.setInt(1, id);
        ResultSet rs = getCategoryById.executeQuery();
        Category ctg = null;
        if (rs.next()) {
            ctg = makeOne(rs);
        }
        rs.close();
        return ctg;
    }

    @Override
    public Category getCategoryByName(String name, CategoryEnum ctgEnum) throws SQLException {
        getCategoryByName.setString(1, name);
        getCategoryByName.setInt(2, ctgEnum.getType());
        ResultSet rs = getCategoryByName.executeQuery();
        Category ctg = null;
        if (rs.next()) {
            ctg = makeOne(rs);
        }
        rs.close();
        return ctg;
    }

    @Override
    public int insert(Category ctg) throws SQLException {
        insert.setString(1, ctg.getName());
        insert.setInt(2, ctg.getCategEnum().getType());
        return insert.executeUpdate();
    }

    @Override
    public int update(Category ctg) throws SQLException {
        update.setString(1, ctg.getName());
        update.setInt(2, ctg.getId());
        return update.executeUpdate();
    }

    @Override
    public void save(Category ctg) throws SQLException {
        if (ctg.getId() == null) {
            insert(ctg);
        } else {
            update(ctg);
        }
    }

    @Override
    public int delete(int id) throws SQLException {
        delete.setInt(1, id);
        return delete.executeUpdate();
    }

    @Override
    public void close() throws SQLException {
        getAllCategories.close();
        getAllCategoriesByType.close();
        getCategoryById.close();
        getCategoryByName.close();
        insert.close();
        update.close();
        delete.close();
    }
    
    @Override
    public Map<Integer, Category> makeCategoryMap() throws SQLException {
        Map<Integer, Category> categMap = new HashMap<>();
        List<Category> categList = getAllCategories();
        for (Category ctg : categList) {
            categMap.put(ctg.getId(), ctg);
        }
        return categMap;
    }

    private List<Category> makeList(ResultSet rs) throws SQLException {
        List<Category> ctgList = new ArrayList<>();
        while (rs.next()) {
            ctgList.add(makeOne(rs));
        }
        return ctgList;
    }

    private Category makeOne(ResultSet rs) throws SQLException {
        Category ctg = new Category();
        ctg.setId(rs.getInt("id"));
        ctg.setName(rs.getString("name"));
        ctg.setCategEnum(getCategoryType(rs.getInt("type")));
        return ctg;
    }
    
    @Override
    public CategoryEnum getCategoryType(int type) {
        switch (type) {
            case 0:
                return CategoryEnum.EXPENSE;
            case 1:
                return CategoryEnum.INCOME;
            default:
                return null;
        }
    }

}
