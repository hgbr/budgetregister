/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import model.entity.Category;
import model.entity.CategoryEnum;

/**
 *
 * @author
 */
public interface CategoryDAO {

    List<Category> getAllCategories() throws SQLException;
    List<Category> getAllCategoriesByType(CategoryEnum ctgEnum) throws SQLException;
    Category getCategoryById(int id) throws SQLException;
    Category getCategoryByName(String name, CategoryEnum ctgEnum) throws SQLException;
    int insert(Category ctg) throws SQLException;
    int update(Category ctg) throws SQLException;
    void save(Category ctg) throws SQLException;
    int delete(int id) throws SQLException;
    void close() throws SQLException;
    Map<Integer, Category> makeCategoryMap() throws SQLException;
    
}
