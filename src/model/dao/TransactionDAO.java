/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import model.entity.Category;
import model.entity.Transaction;

/**
 *
 * @author
 */

public interface TransactionDAO {
    
    List<Transaction> getAllTransactions() throws SQLException;
    List<Transaction> getAllTransByCategory(Optional<Category> categ, Optional<LocalDate> start, Optional<LocalDate> end) throws SQLException;
    List<Transaction> getAllTransByDate(Optional<LocalDate> start, Optional<LocalDate> end) throws SQLException;
    int getBalanceAmount() throws SQLException;
    int insert(Transaction trans) throws SQLException;
    int update(Transaction trans) throws SQLException;
    void save(Transaction trans) throws SQLException;
    int delete(int id) throws SQLException;
    void close() throws SQLException;
    
}
