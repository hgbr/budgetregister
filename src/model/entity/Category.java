/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;


/**
 * Creates a Category object from category table of the database by instantiating this class.
 */
public class Category {
    
    private Integer id;
    private String name;
    private CategoryEnum categEnum;
//    private List<Transaction> trans;
    
    
    public Category() {
    }
    
    public Category(String name, CategoryEnum ctgEnum) {
        this.name = name;
        this.categEnum = ctgEnum;
    }

    public Category(Integer id, String name, CategoryEnum ctgEnum) {
        this.id = id;
        this.name = name;
        this.categEnum = ctgEnum;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryEnum getCategEnum() {
        return categEnum;
    }

    public void setCategEnum(CategoryEnum categEnum) {
        this.categEnum = categEnum;
    }
    
    @Override
    public String toString() {
        return name;
    }

}
