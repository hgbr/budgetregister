/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import com.toedter.calendar.JDateChooser;
import java.time.LocalDate;
import java.time.ZoneId;


/**
 * Creates a Transaction object from transaction table of the database by instantiating this class.
 */
public class Transaction {
    
    private Integer id;
    private Category categ;
    private int amount;
    private LocalDate date;

    
    public Transaction() {
    }

    public Transaction(Category categ, int amount, LocalDate date) {
        this.categ = categ;
        this.amount = amount;
        this.date = date;
    }

    public Transaction(Integer id, Category categ, int amount, LocalDate date) {
        this.id = id;
        this.categ = categ;
        this.amount = amount;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Category getCateg() {
        return categ;
    }

    public void setCateg(Category categ) {
        this.categ = categ;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    
    public void setDateFromChooser(JDateChooser date) {
        this.date = date.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

}
