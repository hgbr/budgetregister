/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.EnumMap;


/**
 * This class manages the calculation of the monthly balance amount.
 */
public class MonthlyData implements Comparable<MonthlyData> {

    private YearMonth date;
    private EnumMap<CategoryEnum, Integer> enumMap;

    
    public MonthlyData() {
        createNewEnumMap();
    }

    public MonthlyData(YearMonth date) {
        this.date = date;
        createNewEnumMap();
    }
    
    public MonthlyData(LocalDate date) {
        setDate(date);
        createNewEnumMap();
    }
    
    public void addCategory(CategoryEnum ctgEnum, int transAmount) {
        enumMap.put(ctgEnum, enumMap.getOrDefault(ctgEnum, 0) + transAmount);
    }
    
    public YearMonth getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = YearMonth.from(date);
    }

    public EnumMap<CategoryEnum, Integer> getEnumMap() {
        return enumMap;
    }

    public void createNewEnumMap() {
        enumMap = new EnumMap<>(CategoryEnum.class);
    }

    @Override
    public int compareTo(MonthlyData o) {
        return o.date.compareTo(date);
    }
    
}
