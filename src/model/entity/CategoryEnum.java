/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;


/**
 * Enumerates the types of category.
 */
public enum CategoryEnum {

    INCOME(1), EXPENSE(0);

    private int type;

    private CategoryEnum(int type) {
        this.type = type;
    }
    
    public int getType() {
        return type;
    }
    
}
