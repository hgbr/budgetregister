/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BudgetRegister;

import model.entity.Category;
import model.entity.CategoryEnum;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author
 */
public class CategoryTest {
    
    Category category;
    
    public CategoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        category = new Category();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test
    public void constructorGetterTestIncome() {
        Category ctg = new Category("Fizetés", CategoryEnum.INCOME);
        assertEquals("Fizetés", ctg.getName());
        assertEquals(CategoryEnum.INCOME, ctg.getCategEnum());
        assertEquals(1, CategoryEnum.INCOME.getType());
        assertTrue(ctg.getCategEnum() == CategoryEnum.INCOME);
    }
    
    @Test
    public void constructorGetterTestExpense() {
        Category ctg = new Category("Élelmiszer", CategoryEnum.EXPENSE);
        assertEquals("Élelmiszer", ctg.getName());
        assertEquals(CategoryEnum.EXPENSE, ctg.getCategEnum());
        assertEquals(0, CategoryEnum.EXPENSE.getType());
        assertTrue(ctg.getCategEnum() == CategoryEnum.EXPENSE);
    }
    
    @Test
    public void setterGetterTestIncome() {
        category.setName("Fizetés");
        category.setCategEnum(CategoryEnum.INCOME);
        assertEquals("Fizetés", category.getName());
        assertEquals(CategoryEnum.INCOME, category.getCategEnum());
        assertEquals("INCOME", category.getCategEnum().name());
        assertEquals(1, CategoryEnum.INCOME.getType());
    } 
    
    @Test
    public void setterGetterTestExpense() {
        category.setName("Élelmiszer");
        category.setCategEnum(CategoryEnum.EXPENSE);
        assertEquals("Élelmiszer", category.getName());
        assertEquals(CategoryEnum.EXPENSE, category.getCategEnum());
        assertEquals(0, CategoryEnum.EXPENSE.getType());
        assertNotEquals(1, CategoryEnum.EXPENSE.getType());
    }
    
}
